FROM node:10.9.0-alpine


ENV PORT 4200
EXPOSE 4200

WORKDIR /usr/src/app
COPY package.json .
RUN npm install -g @angular/cli@6.2.2
RUN npm install -g
RUN npm install --only='dev'
COPY . .
CMD ng serve --host 0.0.0.0 --port 4200 --disableHostCheck true

