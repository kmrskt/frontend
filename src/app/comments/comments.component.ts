import { Component, OnInit } from '@angular/core';
import {CommentsService} from '../comments.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  
  comments:any= [];
  public data:string="";
  constructor(private commentservice : CommentsService) { 
    this.commentservice.getComments().then( (res) =>{
      this.comments=res;
    });
    //console.log("avisha",this.commentservice.getComments());
      //this.comments=this.commentservice.getComments();
  }

  ngOnInit() {
    
    
  }

  addcomment(){
    console.log("input coment",this.data);
    if(this.data.length >0)
    {

      this.commentservice.addcomment(this.data).then( (res) =>{

      this.commentservice.getComments().then( (res) =>{

      this.comments=res;
      this.data="";
    });
      
    });
    }
    
    
    
  }

  addvotes(comment){

    this.commentservice.addvotes(comment).then( (res) =>{

      this.commentservice.getComments().then( (res) =>{
      this.comments=res;
    });
      //this.getComments();
    });
    // upvote
    console.log(comment._id);
  }
 


}
