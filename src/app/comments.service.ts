import { Injectable } from '@angular/core';
import * as _ from 'underscore'
import { environment } from '../environments/environment';
import { Http, Response, RequestOptions, Headers,URLSearchParams,RequestMethod ,ResponseContentType} from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class CommentsService {



  constructor(private http: Http) { }
  getComments(){
    console.log("db get comment");
   let promise = new Promise((resolve, reject) => {
    // 
    let rurl= environment.BASE_URL_BE+"getData";
    //let rurl ="http://localhost:3003/commentpostingsystem/detail/getData"; 
    
    let requestOptions = new RequestOptions();
    this.http.get(rurl,requestOptions)
      .toPromise()
      .then(
        res => { // Success
        //return  res.json(); 
        resolve(res.json().reverse());

        },
        msg => { // Error
        reject(msg);
        }
      );
  });
  return promise;

  }

  addvotes(comment){
   let promise = new Promise((resolve, reject) => {
     // a=f(b)
     // wait till a ;
     // print(a)
     let rurl =environment.BASE_URL_BE +"upvote"; 
    //let rurl ="http://localhost:3003/commentpostingsystem/detail/upvote"; 
    
    let requestOptions = new RequestOptions();
    let params={"id":comment._id};
    this.http.post(rurl,params,requestOptions)
      .toPromise()
      .then(
        res => { // Success
        //return  res.json(); 
        
        console.log(res);
        resolve(res);
        
        },
        msg => { // Error
        reject(msg);
        }
      );
  });

  return promise;

  }

addcomment(comment){
   let promise = new Promise((resolve, reject) => {
     
    let rurl =environment.BASE_URL_BE+"saveData";  
    //let rurl ="http://localhost:3003/commentpostingsystem/detail/saveData"; 
    
    let requestOptions = new RequestOptions();
    let params={"comment":comment}
    this.http.post(rurl,params,requestOptions)
      .toPromise()
      .then(
        res => { // Success
        //return  res.json(); 
        
        resolve(res);
        
        },
        msg => { // Error
        reject(msg);
        }
      );
  });

  return promise;

  }

    

  
  
}
